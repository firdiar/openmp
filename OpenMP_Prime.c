#include<stdio.h>
#include<omp.h>
int main(int argc, char * argv [])
{
    int prime[1000],i,j,n;
    sscanf (argv[1] , " %d " , &n ) ;
	//n = 100;
    for(i=1;i<=n;i++)
    {
        prime[i]=1;
    }
    prime[1]=0;
    for(i=2;i*i<=n;i++)
    {
             /* multi-threading to remove multiples of prime number i from the list (array) */  
     
            #pragma omp parallel for
            for(j=i*i;j<=n;j=j+i)
              {
                  if(prime[j]==1)
                       prime[j]=0;
              }
       
    }
    printf("\nPrime numbers from 1 to %d are\n",n);

    for(i=2;i<=n;i++)
    {
        if(prime[i] == 1)
        {
            printf("%d\t ",i);
        }
    }
    printf("\n");
}